from django.db import models

# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    category = models.ForeignKey('Category')
    tag = models.ManyToManyField('Tag')

    is_model_field = False

    class Meta:
        ordering = ['-created_at','-pk']
        index_together = ['title','created_at']

class Comment(models.Model):
    post = models.ForeignKey(Post)
    content = models.TextField(max_length=500, default=' ')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Tag(models.Model):
    name = models.CharField(max_length=40)

class Category(models.Model):
    name = models.CharField(max_length=40)


