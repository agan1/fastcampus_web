from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.core.paginator import Paginator
from django.core.paginator import PageNotAnInteger
from django.core.paginator import EmptyPage
from django.shortcuts import redirect

from .models import *
# Create your views here.

def hello(request):
    return HttpResponse('hello world')


def hello_with_template(request):
    return render(request,'hello.html')

def list_posts(request):
    per_page = 2
    try:
        current_page = int(request.GET.get('page',1))
    except ValueError:
        current_page = 1

    #Post.objects.prefetch_related()
    #Post.objects.select_related().prefetch_related()
    posts = Post.objects.prefetch_related().all()

    paginator = Paginator(posts, per_page)
    try:
        pg = paginator.page(current_page)
    except EmptyPage:
        pg = []

    posts = posts[(current_page-1)*2:current_page*per_page]
    return render(request,'list_view.html',{
            'posts':pg
        })

def post_detail(request, pk):
        #post = get_object_or_404(Post, pk=pk)
    try:
        post = Post.objects.select_related().prefetch_related().get(pk=pk)
    except Post.DoesNotExist:
        post = None
        #redirect
    return render(request,'post_detail.html', {
        'post': post,
    })

def create_post(request):
    categories = Category.objects.all()

    if request.method == 'GET':
        pass

    if request.method == 'POST':
        new_post = Post()
        new_post.title = request.POST.get('title')
        new_post.content = request.POST.get('content')

        category_pk = request.POST.get('category')
        post_category = Category.objects.get(pk=category_pk)
        
        new_post.category = post_category

        new_post.save()

        return post_detail(request, new_post.pk)

    return render(request, 'create_post.html', {
        'categories' : categories
    })

def create_comment_post(request):
    if request.method == 'POST':
        try:
            post = Post.objects.get(id=request.POST.get('post_pk'))
        except Post.DoesNotExist:
            return None
        new_comment = Comment()
        new_comment.content = request.POST.get('content')
        
        new_comment.post = post
        new_comment.save()

    return redirect('/posts/{}'.format(post.pk))
    
def edit_post(request, pk):
    categories = Category.objects.all()

    post = Post.objects.get(pk=pk)

    if request.method == 'GET':
        pass

    if request.method == 'POST':
        post = Post.objects.get(pk=pk)        
        post.title = request.POST.get('title')
        post.content = request.POST.get('content')

        category_pk = request.POST.get('category')
        post_category = Category.objects.get(pk=category_pk)
        
        post.category = post_category

        post.save()

        return post_detail(request, post.pk)

    return render(request, 'create_post.html', {
        'categories' : categories,
        'post' : post
    })

def delete_post(request, pk):
    post = Post.objects.get(pk=pk)
    post.delete()

    return redirect('/') 
