from django.contrib import admin
# Register your models here.

from .models import *


class CommentInlineAdmin(admin.StackedInline):
    model = Comment
    extra = 1

class PostAdmin(admin.ModelAdmin):
    list_display = ('pk', 'title', 'created_at',)
    list_display_links = ('pk','title', )
    inlines = [CommentInlineAdmin]
    search_fields = ('title', 'content')
    list_filter = ('title', 'created_at')
    date_hierarchy = 'created_at'

admin.site.register(Post, PostAdmin)
admin.site.register(Category)
admin.site.register(Comment)
admin.site.register(Tag)
